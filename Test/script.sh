#!/bin/bash
rm mdrun/*
# creates the topology and structure file
# interactive mode ff: CHARMM27
#gmx pdb2gmx -f water.pdb -o water.gro 

# put the water molecule in a cubic box
#gmx_mpi editconf -f water.gro -o water_box.gro -c -d 1.0 -bt cubic

cd ./mdrun
# creates the binary tpr file which is used as an input for the production run 
gmx_mpi grompp -f ../water_md.mdp -c ../water_box.gro -p ../topol.top -o water_md.tpr -maxwarn 4

# Does the production run
gmx_mpi mdrun -plumed ../plumed.dat -s water_md.tpr -deffnm water_md

# Cojnverts the binary trajectory file to gro file
gmx_mpi trjconv -force -f water_md.trr -o water_md_traj.gro -s water_md.tpr 
gmx_mpi dump -f water_md.trr > water_md_force.txt
