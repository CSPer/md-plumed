
Extending Plumed to enable Hybrid Monte Carlo algorithms to be implemented. 

Current patches : Gromacs 5.0

INSTALLATION

*********** PLUMED compilation
./configure --prefix=installation path CC=mpicc CXX=mpicxx F77-mpif90 FC=mpiF90 LDF90=mpif90 --enable-mpi
make
make install
// Add plumed bin to path

*** PATCHING PLUMED to GROMACS (Do this if you change the patch)
plumed patch -p --shared// select the correct gromacs version from the interactive shell
** To remove the patch 
plumed patch -r 


****Gromacs Compilation
cmake28 .. -DCMAKE_C_COMPILER=icc -DCMAKE_CXX_COMPILER=icpc -DGMX_BUILD_OWN_FFTW_ON -DGMX_MPI=ON -DCMAKE_INSTALL_PREFIX=installation path
make -j 8
make install