#include "Colvar.h"
#include "ActionRegister.h"
#include "tools/Pbc.h"

#include <string>
#include <cmath>
#include <iostream>

using namespace std;

namespace PLMD{
namespace colvar{

//+PLUMEDOC COLVAR VELOCITY
/*
Calculate the components of the velocity of an atom.
\par Examples

\verbatim
# align to a template
v: VELOCITY ATOM=3
PRINT ARG=v.x,v.y,v.z
\endverbatim

*/
//+ENDPLUMEDOC
   
class Velocity : public Colvar {
public:
  static void registerKeywords( Keywords& keys );
  Velocity(const ActionOptions&);
// active methods:
  virtual void calculate();
};

PLUMED_REGISTER_ACTION(Velocity,"VELOCITY")

void Velocity::registerKeywords( Keywords& keys ){
  Colvar::registerKeywords( keys );
  componentsAreNotOptional(keys);
  keys.add("atoms","ATOM","the atom number");
  keys.addOutputComponent("x","default","the x-component of the atom velocity");
  keys.addOutputComponent("y","default","the y-component of the atom velocity");
  keys.addOutputComponent("z","default","the z-component of the atom velocity");
}

Velocity::Velocity(const ActionOptions&ao):
PLUMED_COLVAR_INIT(ao)
{ 
 // ao.getLine();
  vector<AtomNumber> atoms;
  parseAtomList("ATOM",atoms);
  if(atoms.size()!=1)
    error("Number of specified atoms should be 1");
  checkRead();

  log.printf("  for atom %d\n",atoms[0].serial());
  log.printf("  without periodic boundary conditions\n");

  addComponentWithDerivatives("x"); componentIsNotPeriodic("x");
  addComponentWithDerivatives("y"); componentIsNotPeriodic("y");
  addComponentWithDerivatives("z"); componentIsNotPeriodic("z");
  log<<"  WARNING: components will not have the proper periodicity - see manual\n";

  requestAtoms(atoms);
}


// calculator
void Velocity::calculate(){
/*	int r;
	MPI_Comm_rank(MPI_COMM_WORLD,&r);
  printf("Velocity colvar, Processor: %d \n", r);
  Vector vtest;
*/  Vector speed;
  //speed=delta(Vector(0.0,0.0,0.0),getVelocity(0));
  speed= getVelocity(0);
/*  vtest = getVelocity(0);
  printf("getVelocity(0)[0] velocity x: %lf \n",vtest[0]);
  printf("getVelocity(0)[1] velocity x: %lf \n",vtest[1]);
  printf("getVelocity(0)[2] velocity x: %lf \n",vtest[2]);
  printf("speed  velocity x: %lf \n",speed[0]);
  printf("speed  velocity y: %lf \n",speed[1]);
  printf("speed  velocity z: %lf \n",speed[2]); 
*/  Value* valuex=getPntrToComponent("x");
  Value* valuey=getPntrToComponent("y");
  Value* valuez=getPntrToComponent("z");

  setAtomsDerivatives (valuex,0,Vector(+1,0,0));
  setBoxDerivatives   (valuex,Tensor(speed,Vector(-1,0,0)));
  valuex->set(speed[0]);
 // printf("this the value of class value: %lf \n", valuex->get());
  setAtomsDerivatives (valuey,0,Vector(0,+1,0));
  setBoxDerivatives   (valuey,Tensor(speed,Vector(0,-1,0)));
  valuey->set(speed[1]);

  setAtomsDerivatives (valuez,0,Vector(0,0,+1));
  setBoxDerivatives   (valuez,Tensor(speed,Vector(0,0,-1)));
  valuez->set(speed[2]);
  
}


}
}


