#include "MC.h"
#include "core/PlumedMain.h"
#include "ActionRegister.h"

#include <time.h>
#include <stdlib.h>
#include <string>
#include <cmath>
#include <iostream>

using namespace std;

namespace PLMD{
namespace mc{

//+PLUMEDOC COLVAR HMC
/*
Calculate the components of the velocity of an atom.
\par Examples

\verbatim
# align to a template
v: HMC  HMCSTRIDE=5000000 STRIDE=40
PRINT ARG=v.x,v.y,v.z
\endverbatim

*/
//+ENDPLUMEDOC
   
class HMC : public MC {
  double temperature, vBias;
  double oldEtot;
public:
  static void registerKeywords( Keywords& keys );
  HMC(const ActionOptions&);
// active methods:
  void prepare();
  void MCCalculate();
  virtual void calculate();
  double gasdev(double, double);
  double bimodal(double, double);
  double recalculateEnergy(double);
  vector<Vector> generateVelocities();
  void acceptanceTest(double, double);
  void calcEkin();

};

PLUMED_REGISTER_ACTION(HMC,"HMC")

void HMC::registerKeywords( Keywords& keys ){
  MC::registerKeywords( keys );
  keys.add("atoms","ATOM","the atom number");
  keys.add("compulsory","TEMP","the temperature of HMC simulation");
  keys.add("compulsory","vBias","the mean value of velocity distribution");
}

HMC::HMC(const ActionOptions&ao):
PLUMED_MC_INIT(ao)
{ 
//  printf("HMC constructor start\n");	
  parse("TEMP",temperature);
  parse("vBias", vBias);
  //srand48((long)time(NULL));
  isEnergy=true;
//  ao.getLine();
  vector<AtomNumber> atoms;
  parseAtomList("ATOM",atoms);
  checkRead();
  log.printf("  do HMC for the following atoms :");
  for (unsigned i=0; i<atoms.size();++i) log.printf(" %d",atoms[i].serial());
  log.printf("\n");
 
  requestAtoms(atoms);
}

/*********************************/ 
double HMC::gasdev(double mu,double sig)  // gaussian random number "Box-Muller"
{                             /// numerical recipes website 
    static int iset=0;
    static double gset;
    double fac,rsq,v1,v2; 

    if (iset == 0) {
        do {
            v1=2.0*drand48()-1.0;
            v2=2.0*drand48()-1.0;
            rsq=v1*v1+v2*v2;
        } while (rsq >= 1.0 || rsq == 0.0);
        fac=sqrt(-2.0*std::log(rsq)/rsq);
        gset=v1*fac;
        iset=1;
        return sig*((double)(v2*fac))+mu;
    } else {
        iset=0;
        return sig*((double)gset)+mu;
    }
}

double HMC::bimodal(double mu, double sig)
{
	double X;
	X=drand48();
	if (X < 0.5){
		return gasdev(mu, sig);
	}else{
		return gasdev(-mu, sig);
	}
}
/*********************************/ 

void HMC::calcEkin(){
  // calculates the average kinetic energy (leapfrog) using newVelocities and next step cevlocities and masses,
  // assigns the kinetic energy to generatedEkin which is 
  // variable in MC class 
  generatedEkin = 0.0;
  for (unsigned i=0; i<masses.size(); i++) {	
    generatedEkin+= 0.5*masses[i]*Velocities[i][0]*Velocities[i][0];
    generatedEkin+= 0.5*masses[i]*Velocities[i][1]*Velocities[i][1];
    generatedEkin+= 0.5*masses[i]*Velocities[i][2]*Velocities[i][2];
   }
}


vector<Vector> HMC::generateVelocities(){
 //decleration of variables
  vector<Vector> v;
  v.resize(Velocities.size());
  generatedEkin=0.0;
  double X,sig, peak;
  double vcm[3];
  double tm, scl, temp_calc, Ekin;
  int nrdf;

  //initialization
  Ekin=0.0;
  tm =0.0;
  nrdf=0;
  vcm[0]=0.0;
  vcm[1]=0.0;
  vcm[2]=0.0;

  X=drand48();
  if (X < 0.5){
    peak=-1.0;
  }else{
    peak=1.0;
  }
  // loop to draw random number from gaussion distro and calculate the sum of m*v
  for (unsigned i=0;i<v.size();i++){
    sig=sqrt(Boltzau*temperature/(masses[i]));
    v[i][0]=gasdev(peak*vBias,sig);
    v[i][1]=gasdev(peak*vBias,sig);
    v[i][2]=gasdev(peak*vBias,sig);
    Ekin+= 0.5*masses[i]*v[i][0]*v[i][0];
    Ekin+= 0.5*masses[i]*v[i][1]*v[i][1];
    Ekin+= 0.5*masses[i]*v[i][2]*v[i][2];

    vcm[0] += masses[i]*v[i][0];
    vcm[1] += masses[i]*v[i][1];
    vcm[2] += masses[i]*v[i][2];
    
    tm+=masses[i];
    nrdf += 3;
  }
// divide to total mass to get com velocities
  vcm[0]/=tm;
  vcm[1]/=tm;
  vcm[2]/=tm;
  temp_calc = (2.0*Ekin)/(nrdf*Boltzau);
  if (temp_calc > 0){
    scl= sqrt(temperature/temp_calc);
  }else{ 
    scl =1.0;
  } 
// substract the com velocity from each atom's velocity
  /*for (unsigned i=0; i<v.size(); i++){
       // v[i][0]*=scl;
	//v[i][1]*=scl;
	//v[i][2]*=scl;

	v[i][0]-=vcm[0];
	v[i][1]-=vcm[1];
	v[i][2]-=vcm[2];
  }*/
  return v;
}

void HMC::acceptanceTest(double Eold, double Enew){
  // takes the total energy difference as input (kj/mol), calculates 
  // monte carlo acceptance ratio (accept) and set the accepted variable
  // to false if the state is rejected
  //printf("The temperature is %lf\n", temp); 
  double X_rand, accept, accept_bias, deltaE, deltaB;
  double newv, oldv, beta, Pv, Pv_;
  newv = 0.0;
  oldv = 0.0;
  beta = 1.0 / (BoltzAvo*temperature);
  deltaE = -beta*(Enew-Eold);
  for (int i=0; i<22;i++ ){
	for (int j=0; j<3 ; j++){
		newv = newv + beta*vBias*Velocities[i][j]*masses[i];
		oldv = oldv + beta*vBias*oldVelocities[i][j]*masses[i];
  	}
  }
  if (abs(newv) > 20 || abs(oldv) > 20)
  {
    deltaB = abs(newv) - abs(oldv);
    accept = exp(deltaE + deltaB);
  }
  else
  {
    Pv_ = exp(newv) + exp(-newv);
    Pv  = exp(oldv) + exp(-oldv);
    accept_bias = Pv_/Pv;
    accept = exp(deltaE)*accept_bias;
  }
  
  X_rand = drand48();
//c  printf("accept: %lf, X: %lf\n", accept, X_rand);
  // The condition is met if the new state is rejected 
  if ((accept >= 1) || (X_rand < accept)){
	accepted=true; //change to true
  } else {
 	accepted=false; // change to false 
  }
  
  if (accept > 1) { 
	  printf("%ld 1 %ld %lf %lf %lf %lf ", getStep()+1, accepted, X_rand, oldv, newv, vBias);
  }else{
  	printf("%ld %lf %ld %lf %lf %lf %lf ",getStep()+1,accept, accepted, X_rand, oldv, newv, vBias);
  }
}



void HMC::prepare(){
  // sets collectEnergy to True which is used in isEnergyNeeded Plumed cmd command
  plumed.getAtoms().setCollectEnergy(true);
   //printf("im inprepare hmc energy is %lf \n",getEnergy());
}

void HMC::MCCalculate(){

//	printf("*****************Start: MCCalculate***************** \n");
//	printf("Total Energy: %lf\n",getTotEnergy());
	
	double newEtot;
	newEtot=getTotEnergy();
	//printf("total energy: %lf\n" , newEtot);
        if (getStep() == 0){

		printf("Step 	accept    acceptance    X_rand   oldv    newv   vBias  Etot_    Etot\n");
		oldEtot=newEtot;
//c		printf("MCCalculate step 0 Etot: %lf\n",oldEtot);
	}
	else if(onStep_1())
	{
		Velocities=getVelocities();
	/*        for (int i=0; i< 22; i++){
			printf("vels: %lf %lf %lf \n", Velocities[i][0], Velocities[i][1], Velocities[i][2]);
	        }*/
//		printf("deltaE: %lf\n",deltaE);
//c		printf("MCCalculate current Etot: %lf, step-MCstep Etot: %lf\n", newEtot, oldEtot);
		acceptanceTest(oldEtot, newEtot);
		printf("%lf %lf\n", newEtot, oldEtot);
		//printf("%ld %ld\n",getStep()+1, accepted);
		if (!accepted){
		  Positions=oldPositions;
          	}else{
		  oldPositions=Positions;
		}
		MC::MCapply();
			 
	
		//generate new velocities and saved in the plumed local variable. 
		//the velocity of gromacs will be updated beginning of the next step
		//oldVelocities=generateVelocities();

	}else {
//		printf("Step: %ld\n", getStep());
		oldEtot=newEtot;
	}
//	printf("******************End: MCCalculate****************\n");
}

void HMC::calculate(){
  // calculate is executed at a user defined STRIDE which is setted in plumed.dat
  // printf("****************Start: HMC:calculate ********************\n");
//  printf(" Step: %ld\n", getStep());
  if(getStep()==0){
    	masses=getMasses();
        oldPositions=getPositions();
        //calcEkin();
	//printf("generated Ekin: %lf\n", generatedEkin);
  }else if(onStep_1()){
  	Positions=getPositions();
  }
  if(onStep()){
	//printf("new velocites are generated\n");
	Velocities=getVelocities();
//	calcEkin();
//	printf("Calculated Ekin: %lf\n", generatedEkin*Avoau);
        
//	for(int i=0;i<22;i++){
//	  printf(" velocities: %lf %lf %lf \n",newVelocities[i][0],newVelocities[i][1], newVelocities[i][2]);
//	}
	
//	printf("New velocities are generated\n");
	Velocities=generateVelocities();
	//oldVelocities=generateVelocities();
	//calcEkin();
	//printf("generated Ekin: %lf\n", generatedEkin);
//c	printf("calcuted temp: %lf\n", ((2*generatedEkin)/(3*22*Boltzau)));
        /*for (int i=0; i< 22; i++){
		printf("vels: %lf %lf %lf \n", Velocities[i][0], Velocities[i][1], Velocities[i][2]);
	}*/

        oldVelocities=Velocities;
//	printf("Calculated Ekin from genvels: %lf\n", generatedEkin*Avoau);
//	for(int i=0;i<22;i++){
//	  printf("Generated velocities: %lf %lf %lf \n",oldVelocities[i][0],oldVelocities[i][1], oldVelocities[i][2]);
	//}
  }

}

}
}


