#include "MC.h"
#include "core/PlumedMain.h"
#include "ActionRegister.h"

#include <time.h>
#include <stdlib.h>
#include <string>
#include <cmath>
#include <iostream>

using namespace std;

namespace PLMD{
namespace mc{

//+PLUMEDOC COLVAR DFHMC
/*
Calculate the components of the velocity of an atom.
\par Examples

\verbatim
# align to a template
v: HMC  HMCSTRIDE=5000000 STRIDE=40
PRINT ARG=v.x,v.y,v.z
\endverbatim

*/
//+ENDPLUMEDOC
   
class DFHMC : public MC {
  // create variables for the object	
  double temperature;
  double oldEtot;
  int DFStep, DFStride, HMCStride, n, m;
  vector<Vector> deneme, sum;
  bool DF_Active;
public:
  static void registerKeywords( Keywords& keys );
  DFHMC(const ActionOptions&);
// active methods:
  void prepare();
  void MCCalculate();
  virtual void calculate();
  void DF();
  double gasdev(double, double);
  double recalculateEnergy(double);
  vector<Vector> generateVelocities();
  void acceptanceTest(double);
  void calcEkin();

};

PLUMED_REGISTER_ACTION(DFHMC,"DFHMC")

void DFHMC::registerKeywords( Keywords& keys ){
  // the keywords that DFHMC can take from the plumed interface	
  MC::registerKeywords( keys );
  keys.add("atoms","ATOM","the atom number");
  keys.add("compulsory","TEMP","the temperature of HMC simulation");
  keys.add("compulsory","DFStride","the stride of DF application, in md steps");
  keys.add("compulsory","DFStep","the number of filter coefficients");
}

DFHMC::DFHMC(const ActionOptions&ao):
PLUMED_MC_INIT(ao)
{ 
 // DFHMC constructor, do initializations	
//  printf("HMC constructor start\n");	
  parse("TEMP",temperature);
  parse("DFStride",DFStride);
  parse("DFStep",DFStep);
  HMCStride = getStride();
  //srand48((long)time(NULL));
  DF_Active=false;
  n=0;
  m=0;
  isEnergy=true;
//  ao.getLine();
  vector<AtomNumber> atoms;
  parseAtomList("ATOM",atoms);
  checkRead();
  log.printf("  do DFHMC for the following atoms :");
  sum.resize(atoms.size());
  for (unsigned i=0; i<atoms.size();++i){
	  log.printf(" %d",atoms[i].serial());
	  sum[i][0] = 0.0;
	  sum[i][1] = 0.0;
	  sum[i][2] = 0.0;
  }

  log.printf("\n");
 
  requestAtoms(atoms);
}

/*********************************/ 
double DFHMC::gasdev(double mu,double sig)  // gaussian random number "Box-Muller"
{                             /// numerical recipes website 
    static int iset=0;
    static double gset;
    double fac,rsq,v1,v2; 

    if (iset == 0) {
        do {
            v1=2.0*drand48()-1.0;
            v2=2.0*drand48()-1.0;
            rsq=v1*v1+v2*v2;
        } while (rsq >= 1.0 || rsq == 0.0);
        fac=sqrt(-2.0*std::log(rsq)/rsq);
        gset=v1*fac;
        iset=1;
        return sig*((double)(v2*fac))+mu;
    } else {
        iset=0;
        return sig*((double)gset)+mu;
    }
}
/*********************************/ 

void DFHMC::calcEkin(){
  // calculates the average kinetic energy (leapfrog) using newVelocities and next step cevlocities and masses,
  // assigns the kinetic energy to generatedEkin which is 
  // variable in MC class 
  generatedEkin = 0.0;
  for (unsigned i=0; i<masses.size(); i++) {	
    generatedEkin+= 0.5*masses[i]*Velocities[i][0]*Velocities[i][0];
    generatedEkin+= 0.5*masses[i]*Velocities[i][1]*Velocities[i][1];
    generatedEkin+= 0.5*masses[i]*Velocities[i][2]*Velocities[i][2];
   }
}


vector<Vector> DFHMC::generateVelocities(){
 //decleration of variables
  vector<Vector> v;
  v.resize(Velocities.size());
  generatedEkin=0.0;
  double mu;
  double sig;
  double vcm[3];
  double tm, scl, temp_calc, Ekin;
  int nrdf;

  //initialization
  Ekin=0.0;
  tm =0.0;
  nrdf=0;
  vcm[0]=0.0;
  vcm[1]=0.0;
  vcm[2]=0.0;

  // loop to draw random number from gaussion distro and calculate the sum of m*v
  for (unsigned i=0;i<v.size();i++){
    mu=0.0;
    sig=sqrt(Boltzau*temperature/(masses[i]));
    v[i][0]=gasdev(mu,sig);
    v[i][1]=gasdev(mu,sig);
    v[i][2]=gasdev(mu,sig);
    Ekin+= 0.5*masses[i]*v[i][0]*v[i][0];
    Ekin+= 0.5*masses[i]*v[i][1]*v[i][1];
    Ekin+= 0.5*masses[i]*v[i][2]*v[i][2];

    vcm[0] += masses[i]*v[i][0];
    vcm[1] += masses[i]*v[i][1];
    vcm[2] += masses[i]*v[i][2];
    
    tm+=masses[i];
    nrdf += 3;
  }
// divide to total mass to get com velocities
  vcm[0]/=tm;
  vcm[1]/=tm;
  vcm[2]/=tm;
  temp_calc = (2.0*Ekin)/(nrdf*Boltzau);
  if (temp_calc > 0){
    scl= sqrt(temperature/temp_calc);
  }else{ 
    scl =1.0;
  } 
// substract the com velocity from each atom's velocity
  for (unsigned i=0; i<v.size(); i++){
       // v[i][0]*=scl;
	//v[i][1]*=scl;
	//v[i][2]*=scl;

	v[i][0]-=vcm[0];
	v[i][1]-=vcm[1];
	v[i][2]-=vcm[2];
  }
  return v;
}

void DFHMC::acceptanceTest(double deltaE){
  // takes the total energy difference as input (kj/mol), calculates 
  // monte carlo acceptance ratio (accept) and set the accepted variable
  // to false if the state is rejected
  //printf("The temperature is %lf\n", temp); 
  double X_rand, accept;
  deltaE = deltaE / (BoltzAvo*temperature);
  accept = exp(-deltaE);
  X_rand = drand48();
//c  printf("accept: %lf, X: %lf\n", accept, X_rand);
  // The condition is met if the new state is rejected 
  if ((accept < 1) && (X_rand > accept)){
	accepted=false; //change to false
  } else {
 	accepted=true; // change to true 
  }
  
  if (accept > 1) { 
	  printf("%ld 1 %ld %lf\n", getStep()+1, accepted, X_rand);
  }else{
  	printf("%ld %lf %ld %lf\n",getStep()+1,accept, accepted, X_rand);
  }
}



void DFHMC::prepare(){
  // sets collectEnergy to True which is used in isEnergyNeeded Plumed cmd command
  plumed.getAtoms().setCollectEnergy(true);
   //printf("im inprepare hmc energy is %lf \n",getEnergy());
}
void DFHMC::DF(){
	if (getStep() == m || getStride()==1 || getStep()==0){
	        DF_Active = true;
		chStride(1);
		printf("DF : filling buffer and filtrering it.. step: %ld \n", getStep());
		std::cout << deneme.size() << '\n';
	        for (int i=0; i< 22; i++){
			printf("vels in DF: x=%lf y=%lf z=%lf\n", deneme[i][0], deneme[i][1], deneme[i][2]);
		}
		printf("DFStride: %ld\n", DFStride);
		for (int i=0; i< 22; i++){
			sum[i][0] = deneme[i][0] + sum[i][0];
			sum[i][1] = deneme[i][1] + sum[i][1];
			sum[i][2] = deneme[i][2] + sum[i][2];
			printf("sumx= %lf, sumy=%lf, sumy=%lf \n", sum[i][0], sum[i][1], sum[i][2]);
		}
		n+=1;
		if ((n-1) == DFStep){
		        for (int i=0;i<22 ;i++ ) {
				sum[i][0] /= DFStep;
				sum[i][1] /= DFStep;
				sum[i][2] /= DFStep;
			        printf("avrg sumx=%lf, sumy%lf, sumz=%lf\n", sum[i][0], sum[i][1], sum[i][2]);
			}
	
			m = getStep() + DFStride*HMCStride;
			chStride(getStep()+HMCStride);
			n=0;
			DF_Active = false;
		}
	}
}
void DFHMC::MCCalculate(){
	DF();
	if (!DF_Active){
		printf("Do HMC ...\n");
	        if (onStep()){
			printf("changing m \n");
			chStride(getStep() + HMCStride);
		}
		
	}

	/*
//	printf("*****************Start: MCCalculate***************** \n");
//	printf("Total Energy: %lf\n",getTotEnergy());
	
	double deltaE, newEtot;
	newEtot=getTotEnergy();
        if (getStep() == 0){
		printf("Step 	accept    acceptance    X_rand\n");
		oldEtot=newEtot;
//c		printf("MCCalculate step 0 Etot: %lf\n",oldEtot);
	}
	else if(onStep_1())
	{
	 	deltaE = newEtot - oldEtot;
//		printf("deltaE: %lf\n",deltaE);
//c		printf("MCCalculate current Etot: %lf, step-MCstep Etot: %lf\n", newEtot, oldEtot);
		acceptanceTest(deltaE);
		//printf("%ld %ld\n",getStep()+1, accepted);
		if (!accepted){
		  Positions=oldPositions;
          	}else{
		  oldPositions=Positions;
		}
		MC::MCapply();
			 
	
		//generate new velocities and saved in the plumed local variable. 
		//the velocity of gromacs will be updated beginning of the next step
		//oldVelocities=generateVelocities();

	}else {
//		printf("Step: %ld\n", getStep());
		oldEtot=newEtot;
	}
//	printf("******************End: MCCalculate****************\n");
*/
}

void DFHMC::calculate(){
        printf("--calculate--\n");
	deneme = getVelocities();
	/*
  	// calculate is executed at a user defined STRIDE which is setted in plumed.dat
//  printf("****************Start: HMC:calculate ********************\n");
//  printf(" Step: %ld\n", getStep());
  if(getStep()==0){
    	masses=getMasses();
        oldPositions=getPositions();
	//newVelocities.resize(oldPositions.size());
	oldVelocities.resize(oldPositions.size());
  }else if(onStep_1()){
  	Positions=getPositions();
  }
  if(onStep()){
	Velocities=getVelocities();
//	calcEkin();
//	printf("Calculated Ekin: %lf\n", generatedEkin*Avoau);
        
//	for(int i=0;i<22;i++){
//	  printf(" velocities: %lf %lf %lf \n",newVelocities[i][0],newVelocities[i][1], newVelocities[i][2]);
//	}
	
//	printf("New velocities are generated\n");
	Velocities=generateVelocities();
	//oldVelocities=generateVelocities();
//	calcEkin();
//c	printf("Ek generated vel: %lf\n", generatedEkin);
//c	printf("calcuted temp: %lf\n", ((2*generatedEkin)/(3*22*Boltzau)));

//	printf("Calculated Ekin from genvels: %lf\n", generatedEkin*Avoau);
        oldVelocities=Velocities;
//	for(int i=0;i<22;i++){
//	  printf("Generated velocities: %lf %lf %lf \n",oldVelocities[i][0],oldVelocities[i][1], oldVelocities[i][2]);
	//}
  }
*/
}

}
}


