#include "ActionAtomistic.h"
#include "ActionPilot.h"

#ifndef __PLUMED_core_MC_h
#define __PLUMED_core_MC_h

#define PLUMED_MC_INIT(ao) Action(ao), MC(ao)

#include <vector>
namespace PLMD{

class MC:
  public ActionAtomistic,
  public ActionPilot
  {
protected:
  std::vector<double> masses;
  std::vector<Vector> oldVelocities;
  std::vector<Vector> Velocities;
  std::vector<Vector> Positions;
  std::vector<Vector> oldPositions;
  double Temperature;
  double oldEnergy;
  double newEnergy;
  static const double Avoau=0.99999999999;//au*Na *10^3. to get energy in kj/mol
  static const double Boltzau=0.008314455742;// k_B [m^2 s^-2 kg][ 1u / 1.6605402e-27 kg ]  unit conversion from kg to u , where u is atomic unit of mass
  static const double BoltzAvo=8.3144712e-3; // (k_B * NA) or (boltzmanConst * AvagadroNum) in kJ/(mol.K)
  double generatedEkin;
  bool accepted;
  bool isEnergy;
  void requestAtoms( const std::vector<AtomNumber> & a );
  void apply();
  void MCapply();
public:
  MC( const ActionOptions& );
  ~MC(){}
  static void registerKeywords( Keywords& keys );

  };













}

#endif
