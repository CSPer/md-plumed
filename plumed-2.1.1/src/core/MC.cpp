#include "MC.h"

using namespace std;
namespace PLMD{

MC::MC( const ActionOptions&ao):
Action(ao),
ActionAtomistic(ao),
ActionPilot(ao)
{
  accepted=true;
  Temperature=300;
 // printf("MC constructor start\n");
}

void MC::registerKeywords( Keywords& keys ){
  Action::registerKeywords( keys );
  ActionAtomistic::registerKeywords( keys );
  ActionPilot::registerKeywords( keys );
  keys.add("hidden","STRIDE","the number of MD steps before an MC test is applied");
}

void MC::requestAtoms( const vector<AtomNumber> & a){
  ActionAtomistic::requestAtoms(a);
  // May need to resize the forces here
}

void MC::MCapply(){

  vector<Vector>& p(modifyPositions());
  for(unsigned i=0;i<p.size();i++){
    p[i][0]=Positions[i][0];
    p[i][1]=Positions[i][1];
    p[i][2]=Positions[i][2];
  }
  applyPositions();	//updates the position variable in Atoms and sets the value to the pointer to gromacs pos

}

void MC::apply(){
 // assign the generated velocity values to the variable in Atoms.h
 if (onStep()){
  // printf("MC apply: step: %ld\n", getStep());	 
   vector<Vector>& v(modifyVelocities());
   for(unsigned i=0;i<v.size();i++){
     v[i][0]=Velocities[i][0];
     v[i][1]=Velocities[i][1];
     v[i][2]=Velocities[i][2];
   //  printf("plumed velocities: %lf %lf %lf\n", v[i][0],v[i][1],v[i][2]);
   }
   applyVelocities();
  // printf("****************************\n");
 }

}


}
